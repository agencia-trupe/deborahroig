import axios from 'axios';

import AjaxSetup from './AjaxSetup';
import MobileToggle from './MobileToggle';

AjaxSetup();
MobileToggle();

$('.banners').cycle({
    slides: '>.slide',
});

$('.scroll-down').click(event => {
    event.preventDefault();

    $('html, body').animate(
        {
            scrollTop: $('.banners').offset().top + $('.banners').height(),
        },
        1000
    );
});

window.Rellax('.rellax');

function getInstagramImages() {
    const url = 'https://www.instagram.com/deborahroig8/?__a=1';

    axios.get(url).then(({ data }) => {
        try {
            const { edges } = data.graphql.user.edge_owner_to_timeline_media;

            const pictures = edges.slice(0, 6).map(({ node }) => ({
                url: `https://instagram.com/p/${node.shortcode}`,
                thumbnail: node.thumbnail_resources[4].src,
            }));

            const $picturesDiv = $(`<div class="pictures"></div>`).append(
                pictures.map(
                    p =>
                        `<a href="${p.url}" target="_blank"><img src="${p.thumbnail}" /></a>`
                )
            );

            $('.home .instagram .center').append($picturesDiv);
        } catch (err) {
            console.error(err);
        }
    });
}

if ($('.home .instagram').length) {
    getInstagramImages();
}

function prepareMedia() {
    $('.fancybox-video')
        .off('click.fb-start')
        .fancybox({
            live: false,
            padding: 0,
            type: 'iframe',
            width: 800,
            height: 450,
            aspectRatio: true,
        });

    $('.fancybox')
        .off('click.fb-start')
        .fancybox({
            live: false,
            padding: 30,
        });
}

$(document).on('click', '.fancybox-gallery', function gallery(event) {
    event.preventDefault();

    const id = $(this).data('gallery');

    if (id) $(`.fancybox[rel=${id}]:eq(0)`).click();
});

prepareMedia();

$('.load-more').click(function loadMore(event) {
    event.preventDefault();

    if ($(this).hasClass('loading')) return;

    $(this).addClass('loading');

    axios
        .get($(this).data('next'))
        .then(({ data }) => {
            const { view, next } = data;

            const $container = $(this).prev();
            const $items = $(view);

            $container.append($items);

            if ($container.hasClass('masonry')) {
                $container.masonry('appended', $items);
                $container.imagesLoaded().progress(() => {
                    $container.masonry('layout');
                });
            }

            prepareMedia();

            if (next) {
                $(this).data('next', next);
            } else {
                $(this).remove();
            }
        })
        .finally(() => $(this).removeClass('loading'));
});

const $grid = $('.masonry');

if ($grid.length) {
    $grid.masonry({
        itemSelector: '.thumb',
        columnWidth: '.sizer',
        gutter: '.gutter',
        percentPosition: true,
    });

    $grid.imagesLoaded(() => {
        $grid.masonry('layout');
    });
}

function positionCategorias() {
    const $wrapper = $('.categorias');
    const $active = $wrapper.find('.active');

    const translate =
        $active.width() / 2 + $active.offset().left - $wrapper.offset().left;

    $wrapper.css('transform', `translateX(-${translate}px)`);
}

if ($('.categorias').length) {
    positionCategorias();
    setInterval(positionCategorias, 200);
}
