    <footer>
        <div class="center">
            <div class="footer-contato">
                <div class="footer-redes">
                        @if($contato->whatsapp)
                            <a href="https://api.whatsapp.com/send?phone=55{{ $contato->whatsapp }}" class="whatsapp" target="_blank"><p class="footer_tel">{{ $contato->telefone }}</p></a>
                        @elseif(count($contato->whatapp) == 0)
                            <p>{{ $contato->telefone }}</p>
                        @endif 
                </div>
                <div class="footer-redes">
                    @if($contato->instagram)
                    <a href="{{ $contato->instagram }}" class="instagram" target="_blank">instagram</a>
                    @endif
                    @if($contato->facebook)
                    <a href="{{ $contato->facebook }}" class="facebook" target="_blank"></a>
                    @endif
                </div>
            </div>

            <div class="footer-endereco">{!! $contato->endereco !!}</div>

            <img src="{{ asset('assets/img/layout/marca-deborahroigbege.svg') }}" class="footer-logo" alt="">

            <div class="footer-links">
                <div>
                    <a href="{{ route('home') }}">HOME</a>
                    <a href="{{ route('sobre') }}">SOBRE</a>
                </div>
                <div>
                    <a href="{{ route('projetos') }}">PROJETOS</a>
                    @foreach($projetosCategorias as $c)
                    <a href="{{ route('projetos', $c->slug) }}" class="sub">// {{ $c->titulo }}</a>
                    @endforeach
                    <a href="{{ route('projetos.sustentabilidade') }}" class="sub">// sustentabilidade</a>
                </div>
                <div>
                    <a href="{{ route('midia') }}">MÍDIA</a>
                    <a href="{{ route('videos') }}">VÍDEOS</a>
                    <a href="{{ route('contato') }}">CONTATO</a>
                </div>
            </div>
        </div>

        <div class="copyright">
            <div class="center">
                <a href="{{ route('politica-de-privacidade') }}" target="_blank">POLÍTICA DE PRIVACIDADE</a>
                &copy; {{ config('app.name') }} {{ date('Y') }} &middot; Todos os direitos reservados
                <span>|</span>
                <a href="https://www.trupe.net" target="_blank">Criação de sites</a>:
                <a href="https://www.trupe.net" target="_blank">Trupe Agência Criativa</a>
            </div>
        </div>
    </footer>
