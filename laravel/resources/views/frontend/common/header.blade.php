    <header @if(Tools::routeIs('home')) class="header-home" @endif>
        <div class="center">
            <a href="{{ route('home') }}" class="logo">
                <img src="{{ asset('assets/img/layout/marca-deborahroigbege.svg') }}" alt="">
            </a>
            <nav id="nav-desktop">
                <a href="{{ route('home') }}" @if(Tools::routeIs('home')) class="active" @endif>
                    HOME
                </a>
                <a href="{{ route('projetos') }}" @if(Tools::routeIs('projetos*')) class="active" @endif>
                    PROJETOS
                </a>
                <a href="{{ route('midia') }}" @if(Tools::routeIs('midia')) class="active" @endif>
                    MÍDIA
                </a>
                <a href="{{ route('videos') }}" @if(Tools::routeIs('videos')) class="active" @endif>
                    VÍDEOS
                </a>
                <a href="{{ route('sobre') }}" @if(Tools::routeIs('sobre')) class="active" @endif>
                    SOBRE
                </a>
                <a href="{{ route('contato') }}" @if(Tools::routeIs('contato')) class="active" @endif>
                    CONTATO
                </a>

                @if($contato->instagram)
                    <a href="{{ $contato->instagram }}" class="instagram" target="_blank">instagram</a>
                @endif
                @if($contato->facebook)
                    <a href="{{ $contato->facebook }}" class="facebook" target="_blank"></a>
                @endif
                @if($contato->whatsapp)
                    <a href="https://api.whatsapp.com/send?phone=55{{ $contato->whatsapp }}" class="whatsapp" target="_blank"></a>
                @endif
            </nav>
            <button id="mobile-toggle" type="button" role="button">
                <span class="lines"></span>
            </button>
        </div>
    </header>

    <nav id="nav-mobile">
        <div class="center">
            <a href="{{ route('home') }}" @if(Tools::routeIs('home')) class="active" @endif>
                HOME
            </a>
            <a href="{{ route('projetos') }}" @if(Tools::routeIs('projetos*')) class="active" @endif>
                PROJETOS
            </a>
            <a href="{{ route('midia') }}" @if(Tools::routeIs('midia')) class="active" @endif>
                MÍDIA
            </a>
            <a href="{{ route('sobre') }}" @if(Tools::routeIs('sobre')) class="active" @endif>
                SOBRE
            </a>
            <a href="{{ route('contato') }}" @if(Tools::routeIs('contato')) class="active" @endif>
                CONTATO
            </a>

            @if($contato->instagram)
                <a href="{{ $contato->instagram }}" class="instagram" target="_blank">instagram</a>
            @endif
            @if($contato->facebook)
                <a href="{{ $contato->facebook }}" class="facebook" target="_blank"></a>
            @endif
            @if($contato->whatsapp)
                <a href="{{ $contato->whatsapp }}" class="whatsapp" target="_blank"></a>
            @endif
        </div>
    </nav>
