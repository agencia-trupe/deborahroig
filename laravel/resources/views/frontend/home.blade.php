@extends('frontend.common.template')

@section('content')

    <div class="home">
        <div class="banners">
            @foreach($banners as $b)
                @if($b->link)
                <a href="{{ Tools::parseLink($b->link) }}" class="slide" style="background-image:url({{ asset('assets/img/banners/'.$b->imagem) }})">
                    @if($b->projeto)
                    <span class="projeto">{{ $b->projeto }}</span>
                    @endif
                </a>
                @else
                <div class="slide" style="background-image:url({{ asset('assets/img/banners/'.$b->imagem) }})">
                    @if($b->projeto)
                    <span class="projeto">{{ $b->projeto }}</span>
                    @endif
                </div>
                @endif
            @endforeach

            <img src="{{ asset('assets/img/layout/marca-deborahroig.svg') }}" class="banners-logo" alt="">
            <a href="#" class="cycle-prev"></a>
            <a href="#" class="cycle-next"></a>
            <a href="#" class="scroll-down">SCROLL DOWN</a>
        </div>

        @if($home->frase || $home->texto)
        <div class="texto">
            <div class="center">
                <div>
                    <h2>
                        <span>{{ $home->frase }}</span>
                    </h2>

                    {!! $home->texto !!}
                </div>
            </div>
        </div>
        @endif

        @if($home->video || count($imagens))
        <div class="conteudo center-full @if(!count($imagens)) video-only @endif">
            @if($home->video)
            <div class="video">
                <iframe src="{{ $home->video }}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
            @endif

            @if(count($imagens))
            <div class="imagens">
                @foreach($imagens as $i)
                    @if($i->link)
                    <a href="{{ Tools::parseLink($i->link) }}">
                        <img src="{{ asset('assets/img/imagens/'.$i->imagem) }}" alt="">
                    </a>
                    @else
                    <img src="{{ asset('assets/img/imagens/'.$i->imagem) }}" alt="">
                    @endif
                @endforeach
                @if(count($imagens) % 2)
                <div class="filler"></div>
                @endif
            </div>
            @endif
        </div>
        @endif

        @if(count($destaques))
        <div class="destaques">
            @foreach($destaques as $d)
            <a href="{{ route('projetos.show', [$d->projeto->categoria->slug, $d->projeto->slug]) }}" style="background-image: url({{ asset('assets/img/destaques/'.$d->imagem) }})">
                <div>
                    <div>
                        <span>// PROJETO EM DESTAQUE</span>
                        {{ $d->projeto->titulo }}
                    </div>
                </div>
            </a>
            @endforeach
        </div>
        @endif

        <div class="instagram">
            <div class="center">
                <a href="https://instagram.com/deborahroig8" class="link-instagram" target="_blank">
                    SIGA-NOS
                    <span></span>
                    @deborahroig8
                </a>
            </div>
        </div>

        <div class="chamada-sobre">
            <div class="center">
                <a href="{{ route('sobre') }}">
                    <div>
                        <span>// NOSSA METODOLOGIA DE TRABALHO</span>
                        CONHEÇA MAIS
                    </div>
                    <div>
                        <img src="{{ asset('assets/img/home/'.$home->imagem_sobre_1) }}" alt="">
                        <img src="{{ asset('assets/img/home/'.$home->imagem_sobre_2) }}" alt="">
                    </div>
                </a>
            </div>
        </div>
    </div>

@endsection
