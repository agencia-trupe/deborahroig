@extends('frontend.common.template')

@section('content')

    <div class="midia">
        <div class="center-full">
            <div class="masonry">
                @include('frontend.midia._thumbs', compact($midia))
                <div class="gutter"></div>
                <div class="sizer"></div>
            </div>

            @if($midia->hasMorePages())
            <a href="#" class="load-more" data-next="{{ $midia->nextPageUrl() }}">
                VER MAIS
                <span></span>
            </a>
            @endif
        </div>
    </div>

@endsection
