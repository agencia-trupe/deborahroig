@foreach($midia as $m)
    @if(!$m->video && !$m->link && !count($m->imagens))
    <div class="thumb">
        @include('frontend.midia._thumb-content', compact('m'))
    </div>
    @elseif($m->video)
    <a href="{{ $m->video }}" class="thumb fancybox-video">
        @include('frontend.midia._thumb-content', compact('m'))
    </a>
    @elseif($m->link)
    <a href="{{ Tools::parseLink($m->link) }}" class="thumb" target="_blank">
        @include('frontend.midia._thumb-content', compact('m'))
    </a>
    @else
    <a href="#" class="thumb fancybox-gallery" data-gallery="midia-{{ $m->id }}">
        @include('frontend.midia._thumb-content', compact('m'))
    </a>
    <div style="display:none">
        @foreach($m->imagens as $i)
        <a href="{{ asset('assets/img/midia/imagens/'.$i->imagem) }}" class="fancybox" rel="midia-{{ $m->id }}" alt=""></a>
        @endforeach
    </div>
    @endif
@endforeach
