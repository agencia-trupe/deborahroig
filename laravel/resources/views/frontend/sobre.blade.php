@extends('frontend.common.template')

@section('content')

    <div class="sobre">
        <div class="center-full">
            <div class="section-1">
                <div class="imagem" style="background-image:url({{ asset('assets/img/sobre/'.$sobre->imagem_1) }})"></div>
                <div class="box rellax" data-rellax-speed="2">{!! $sobre->texto_1 !!}</div>
            </div>

            <div class="frase">
                <span>{{ $sobre->frase }}</span>
            </div>

            <div class="section-2">
                <div class="imagem" style="background-image:url({{ asset('assets/img/sobre/'.$sobre->imagem_2) }})"></div>
                <div class="box rellax" data-rellax-speed="2">{!! $sobre->texto_2 !!}</div>
            </div>

            <div class="section-3">
                <div class="imagem" style="background-image:url({{ asset('assets/img/sobre/'.$sobre->imagem_3) }})"></div>
                <div class="box rellax" data-rellax-speed="1.5">{!! $sobre->texto_3 !!}</div>
            </div>

            <div class="section-4 rellax" data-rellax-speed="0.5">
                <img src="{{ asset('assets/img/sobre/'.$sobre->imagem_4) }}" alt="">
                <div class="box">{!! $sobre->texto_4 !!}</div>
            </div>
        </div>
    </div>

@endsection
