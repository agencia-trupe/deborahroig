@extends('frontend.common.template')

@section('content')

    <div class="contato">
        <div class="center-full">
            <div class="form-wrapper">
                <div class="info">
                    <div class="contato-whatsapp">
                        @if($contato->whatsapp)
                        <a href="https://api.whatsapp.com/send?phone=55{{ $contato->whatsapp }}" class="whatsapp"><p class="telefone" target="_blank">{{ $contato->telefone }}</p></a>
                        <!-- <a href="https://api.whatsapp.com/send?phone=55{{ $contato->whatsapp }}" class="telefone" id="telefone"></a> -->
                        @elseif(count($contato->whatapp) == 0)
                        <p>{{ $contato->telefone }}</p>
                        @endif
                    </div>
                    <a href="mailto:{{ $contato->email }}" class="email">{{ $contato->email }}</a>
                </div>

                <form action="{{ route('contato.post') }}" method="POST">
                    {!! csrf_field() !!}
                    @if(session('enviado'))
                    <div class="flash flash-success">Mensagem enviada com sucesso!</div>
                    @elseif($errors->any())
                    <div class="flash flash-error">Preencha todos os campos corretamente.</div>
                    @endif

                    <div class="grid">
                        <div>
                            <input type="text" name="nome" placeholder="nome" value="{{ old('nome') }}" required>
                            <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
                            <input type="text" name="telefone" placeholder="telefone" value="{{ old('telefone') }}">
                        </div>
                        <textarea name="mensagem" placeholder="mensagem" required>{{ old('mensagem') }}</textarea>
                        <button type="button" id="activate_captcha" class="buttons"></button>
                        <div class="active_captcha">
                            <div class="card">
                                {!! Recaptcha::render() !!}
                                <button type="submit" class="submit">Enviar</button>
                                <button type="button" id="deactivate_captcha" class="btn-close"></button>
                            </div>
                        </div>

                        @if($errors->any())
                        <div class="flash flash-erro">
                            @foreach($errors->all() as $error)
                            {!! $error !!}<br>
                            @endforeach
                            @if($errors->has('g-recaptcha-response'))
                            <br>Recaptcha inválido.
                            @endif
                        </div>
                        @endif
                    </div>
                </form>
            </div>

            <div class="img-wrapper">
                <img src="{{ asset('assets/img/contato/'.$contato->imagem) }}" alt="">
                <div class="mapa">
                    {!! $contato->google_maps !!}
                    <p>{!! $contato->endereco !!}</p>
                </div>
            </div>
        </div>
    </div>

@endsection
