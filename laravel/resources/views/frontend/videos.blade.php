@extends('frontend.common.template')

@section('content')

    <div class="videos">
        <div class="center-full">
           <div class="videos_father">
            @foreach($videos as $video)
                <div class="blogBox moreBox">
                    <div class="overlay">
                        <iframe width="100%" height="100%" src="https://www.youtube.com/embed/{{$video->link}}" 
                            frameborder="0" allowfullscreen></iframe>
                    </div>
                    <div>
                        <h1>{{ $video->titulo }}</h1>
                    </div>
                </div>
            </a>
            @endforeach
            </div>
                <div id="loadMore">
                    <p>VER MAIS +</p>
                </div>
        </div>
    </div>

@endsection
