@foreach($projetos as $p)
<div class="thumb">
    @foreach($p->imagens as $i)
    <img src="{{ asset('assets/img/sustentabilidade/imagens/'.$i->imagem) }}" alt="">
    @endforeach
    @if($p->video)
    <div class="video">
        <iframe src="{{ $p->video }}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
    @endif
    <h2>{{ $p->titulo }}</h2>
    @if($p->descricao)
    <div class="descricao">{!! $p->descricao !!}</div>
    @endif
</div>
@endforeach
