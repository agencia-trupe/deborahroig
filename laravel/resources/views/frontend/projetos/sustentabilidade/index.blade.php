@extends('frontend.common.template')

@section('content')

    <div class="projetos">
        <div class="center-full">
            @include('frontend.projetos._categorias')

            <div class="masonry">
                @include('frontend.projetos.sustentabilidade._projetos', compact($projetos))
                <div class="gutter"></div>
                <div class="sizer"></div>
            </div>

            @if($projetos->hasMorePages())
            <a href="#" class="load-more" data-next="{{ $projetos->nextPageUrl() }}">
                VER MAIS
                <span></span>
            </a>
            @endif
        </div>
    </div>

@endsection
