@extends('frontend.common.template')

@section('content')

    <div class="projetos">
        <div class="center-full">
            @include('frontend.projetos._categorias')

            <div class="masonry projeto_father">
                {{-- <div class="father projeto_father"> --}}

                    @if(count($projeto->videos) > 0)
                    @foreach ($projeto->videos as $v)
                    <div class="video thumb moreBox2" style="padding: 0">
                        <iframe src="{{ $v->video }}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                    @endforeach
                    @endif
                    
                    <div class="first thumb moreBox2" style="padding: 0">
                        <img src="{{ asset('assets/img/projetos/imagens/'.$imagemFirst->imagem) }}" alt="">
                    </div>

                
                    <div class="descricao thumb moreBox2" style="padding: 30px">
                        <span>// {{ $projeto->categoria->titulo }}</span>
                        <h2>{{ $projeto->titulo }}</h2>
                        <span>{{ $projeto->local }}</span>
                        <p>{!! $projeto->descricao !!}</p>
                    </div>
                    
                    @if(count($projeto->imagens))
                        @foreach($projeto->imagens as $i)
                            @if ($i->id != $imagemFirst->id)  
                                <img class="thumb moreBox2" style="padding: 0" src="{{ asset('assets/img/projetos/imagens/'.$i->imagem) }}" alt="">
                            @endif
                        @endforeach
                    @endif

                    <div class="gutter" style="width: 20px"></div>
                    <div class="sizer"></div>

                {{-- </div> --}}
            </div>

            <div id="loadMore2">
                <p>VER MAIS +</p>
            </div>

            <div class="controls">
                <a href="{{ route('projetos', $projeto->categoria->slug) }}">
                    VOLTAR
                </a>

                @if($next)
                <a href="{{ route('projetos.show', [$next->categoria->slug, $next->slug]) }}">
                    PRÓXIMO PROJETO
                </a>
                @endif
            </div>
        </div>
    </div>

    <style>
        .thumb {
            margin-bottom: 20px!important;
            border: 0!important;
        }
    </style>

@endsection
