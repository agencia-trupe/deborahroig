<div class="categorias">
    @foreach($categorias as $c)
    <a href="{{ route('projetos', $c->slug) }}" @if(isset($categoria) && $categoria->slug == $c->slug) class="active" @endif>
        {{ $c->titulo }}
    </a>
    @endforeach
    <a href="{{ route('projetos.sustentabilidade') }}" @if(Tools::routeIs('projetos.sustentabilidade')) class="active" @endif>
        sustentabilidade
    </a>
</div>
