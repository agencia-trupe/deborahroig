@extends('frontend.common.template')

@section('content')

    <div class="projetos">
        <div class="center-full">
            @include('frontend.projetos._categorias')

            <div class="index-grid">
                @foreach($projetos as $p)
                <a href="{{ route('projetos.show', [$p->categoria->slug, $p->slug]) }}">
                    <img src="{{ asset('assets/img/projetos/'.$p->capa) }}" alt="">
                    <div class="overlay">
                        <div>
                            {{ $p->titulo }}
                            <span>// {{ $p->categoria->titulo }}</span>
                        </div>
                    </div>
                </a>
                @endforeach
            </div>
        </div>
    </div>

@endsection
