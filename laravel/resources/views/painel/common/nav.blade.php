<ul class="nav navbar-nav">
    <li class="dropdown @if(Tools::routeIs([
        'painel.home*',
        'painel.imagens*',
        'painel.destaques*',
        ])) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Home
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.home*')) class="active" @endif>
                <a href="{{ route('painel.home.index') }}">Home</a>
            </li>
            <li @if(Tools::routeIs('painel.imagens*')) class="active" @endif>
                <a href="{{ route('painel.imagens.index') }}">Imagens</a>
            </li>
            <li @if(Tools::routeIs('painel.destaques*')) class="active" @endif>
                <a href="{{ route('painel.destaques.index') }}">Destaques</a>
            </li>
        </ul>
    </li>
    <li @if(Tools::routeIs('painel.banners*')) class="active" @endif>
        <a href="{{ route('painel.banners.index') }}">Banners</a>
    </li>
	<li @if(Tools::routeIs('painel.projetos*')) class="active" @endif>
		<a href="{{ route('painel.projetos.index') }}">Projetos</a>
	</li>
    <li @if(Tools::routeIs('painel.sustentabilidade*')) class="active" @endif>
        <a href="{{ route('painel.sustentabilidade.index') }}">Sustentabilidade</a>
    </li>
    <li @if(Tools::routeIs('painel.midia*')) class="active" @endif>
        <a href="{{ route('painel.midia.index') }}">Mídia</a>
    </li>
    <li @if(Tools::routeIs('painel.videos*')) class="active" @endif>
        <a href="{{ route('painel.videos.index') }}">Vídeos</a>
    </li>
	<li @if(Tools::routeIs('painel.sobre*')) class="active" @endif>
		<a href="{{ route('painel.sobre.index') }}">Sobre</a>
	</li>
    <li class="dropdown @if(Tools::routeIs('painel.contato*')) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contato
            @if($contatosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.contato.index')) class="active" @endif>
                <a href="{{ route('painel.contato.index') }}">Informações de Contato</a>
            </li>
            <li @if(Tools::routeIs('painel.contato.recebidos*')) class="active" @endif>
                <a href="{{ route('painel.contato.recebidos.index') }}">
                    Contatos Recebidos
                    @if($contatosNaoLidos >= 1)
                    <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                    @endif
                </a>
            </li>
        </ul>
    </li>
</ul>
