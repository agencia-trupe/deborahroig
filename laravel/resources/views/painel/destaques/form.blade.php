@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('projeto_id', 'Projeto') !!}
    {!! Form::select('projeto_id', $projetos, null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/destaques/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.destaques.index') }}" class="btn btn-default btn-voltar">Voltar</a>
