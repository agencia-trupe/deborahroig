@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Home / Destaques /</small> Editar Destaque</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.destaques.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.destaques.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
