@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Projetos / {{ $projeto->titulo }} /</small> Editar Vídeo</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.projetos.videos.update', $projeto->id, $registro->id],
        'method' => 'patch'])
    !!}

    @include('painel.projetos.videos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
