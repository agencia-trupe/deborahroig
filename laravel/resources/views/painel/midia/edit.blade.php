@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Mídia /</small> Editar Mídia</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.midia.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.midia.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
