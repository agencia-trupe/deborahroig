@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Sustentabilidade /</small> Adicionar Sustentabilidade</h2>
    </legend>

    {!! Form::open(['route' => 'painel.sustentabilidade.store', 'files' => true]) !!}

        @include('painel.sustentabilidade.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
