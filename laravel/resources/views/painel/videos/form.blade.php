@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('titulo', 'Título do Vídeo') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('link', 'Vídeo do YouTube') !!}
    {!! Form::text('link', null, ['class' => 'form-control']) !!}
    <p style="color:red;">Incluir aqui somente a parte da url após o "v=". Exemplo: https://www.youtube.com/watch?v=<strong>Y4goaZhNt4k</strong></p>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.videos.index') }}" class="btn btn-default btn-voltar">Voltar</a>
