<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
		$router->model('destaques', 'App\Models\Destaque');
		$router->model('imagens', 'App\Models\Imagem');
		$router->model('home', 'App\Models\Home');
		$router->model('midia', 'App\Models\Midia');
		$router->model('imagens_midia', 'App\Models\MidiaImagem');
		$router->model('sustentabilidade', 'App\Models\Sustentabilidade');
		$router->model('imagens_sustentabilidade', 'App\Models\SustentabilidadeImagem');
		$router->model('projetos', 'App\Models\Projeto');
		$router->model('categorias_projetos', 'App\Models\ProjetoCategoria');
		$router->model('videos_projetos', 'App\Models\ProjetoVideo');
		$router->model('imagens_projetos', 'App\Models\ProjetoImagem');
		$router->model('banners', 'App\Models\Banner');
        $router->model('videos', 'App\Models\Videos');
		$router->model('sobre', 'App\Models\Sobre');
		$router->model('configuracoes', 'App\Models\Configuracoes');
        $router->model('recebidos', 'App\Models\ContatoRecebido');
        $router->model('contato', 'App\Models\Contato');
        $router->model('usuarios', 'App\Models\User');

        $router->bind('categoria_slug', function($slug) {
            return \App\Models\ProjetoCategoria::whereSlug($slug)->firstOrFail();
        });
        $router->bind('projeto_slug', function($slug) {
            return \App\Models\Projeto::whereSlug($slug)->firstOrFail();
        });

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
