<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Midia;

class MidiaController extends Controller
{
    private $PER_PAGE = 15;

    public function index()
    {
        $midia = Midia::ordenados()->paginate($this->PER_PAGE);

        if (request()->wantsJson()) {
            return response()->json([
                'view' => view('frontend.midia._thumbs', compact('midia'))->render(),
                'next' => $midia->nextPageUrl(),
            ]);
        }

        if (request()->has('page')) abort('404');

        return view('frontend.midia.index', compact('midia'));
    }
}
