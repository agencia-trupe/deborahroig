<?php

namespace App\Http\Controllers;

use App\Models\Imagem;
use App\Models\ProjetoCategoria;
use App\Models\Projeto;
use App\Models\ProjetoImagem;
use App\Models\Sustentabilidade;

class ProjetosController extends Controller
{
    public function __construct()
    {
        view()->share('categorias', ProjetoCategoria::ordenados()->get());
    }

    public function index(ProjetoCategoria $categoria)
    {
        if (! $categoria->exists) {
            $categoria = ProjetoCategoria::ordenados()->firstOrFail();
        }

        view()->share('categoria', $categoria);

        $projetos = $categoria->projetos;

        return view('frontend.projetos.index', compact('projetos'));
    }

    // public function show(ProjetoCategoria $categoria, Projeto $projeto)
    // {
    //     if ($projeto->categoria->id != $categoria->id) {
    //         abort('404');
    //     }

    //     view()->share('categoria', $categoria);


    //     $nextKey = null;
    //     $projetos = $categoria->projetos;
    //     foreach ($projetos as $key => $value) {
    //         if ($value->id == $projeto->id) {
    //             $nextKey = $key + 1;
    //         }
    //     }

    //     $next = $projetos->get($nextKey);

    //     return view('frontend.projetos.show', compact('projeto', 'next'));
    // }

    public function show(ProjetoCategoria $categoria, Projeto $projeto)
    {
        if ($projeto->categoria->id != $categoria->id) {
            abort('404');
        }

        view()->share('categoria', $categoria);


        $nextKey = null;
        $projetos = $categoria->projetos;
        $imagemFirst = ProjetoImagem::where('projeto_id', $projeto->id)->first();
        
        foreach ($projetos as $key => $value) {
            if ($value->id == $projeto->id) {
                $nextKey = $key + 1;
            }
        }

        $next = $projetos->get($nextKey);

        return view('frontend.projetos.show', compact('projeto', 'next', 'imagemFirst'));
    }


    public function sustentabilidade()
    {
        $projetos = Sustentabilidade::ordenados()->paginate(6);

        if (request()->wantsJson()) {
            return response()->json([
                'view' => view('frontend.projetos.sustentabilidade._projetos', compact('projetos'))->render(),
                'next' => $projetos->nextPageUrl(),
            ]);
        }

        if (request()->has('page')) abort('404');

        return view('frontend.projetos.sustentabilidade.index', compact('projetos'));
    }
}
