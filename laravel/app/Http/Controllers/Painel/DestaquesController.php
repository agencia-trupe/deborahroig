<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\DestaquesRequest;
use App\Http\Controllers\Controller;

use App\Models\Destaque;
use App\Models\Projeto;

class DestaquesController extends Controller
{
    public function index()
    {
        $registros = Destaque::ordenados()->get();

        return view('painel.destaques.index', compact('registros'));
    }

    public function create()
    {
        $projetos = Projeto::orderBy('titulo')->lists('titulo', 'id');

        return view('painel.destaques.create', compact('projetos'));
    }

    public function store(DestaquesRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Destaque::upload_imagem();

            Destaque::create($input);

            return redirect()->route('painel.destaques.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Destaque $registro)
    {
        $projetos = Projeto::orderBy('titulo')->lists('titulo', 'id');

        return view('painel.destaques.edit', compact('registro', 'projetos'));
    }

    public function update(DestaquesRequest $request, Destaque $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Destaque::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.destaques.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Destaque $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.destaques.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
