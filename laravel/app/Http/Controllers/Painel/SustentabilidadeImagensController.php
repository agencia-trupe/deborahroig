<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\SustentabilidadeImagensRequest;
use App\Http\Controllers\Controller;

use App\Models\Sustentabilidade;
use App\Models\SustentabilidadeImagem;

use App\Helpers\CropImage;

class SustentabilidadeImagensController extends Controller
{
    public function index(Sustentabilidade $registro)
    {
        $imagens = SustentabilidadeImagem::sustentabilidade($registro->id)->ordenados()->get();

        return view('painel.sustentabilidade.imagens.index', compact('imagens', 'registro'));
    }

    public function show(Sustentabilidade $registro, SustentabilidadeImagem $imagem)
    {
        return $imagem;
    }

    public function store(Sustentabilidade $registro, SustentabilidadeImagensRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = SustentabilidadeImagem::uploadImagem();
            $input['sustentabilidade_id'] = $registro->id;

            $imagem = SustentabilidadeImagem::create($input);

            $view = view('painel.sustentabilidade.imagens.imagem', compact('registro', 'imagem'))->render();

            return response()->json(['body' => $view]);

        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: '.$e->getMessage();

        }
    }

    public function destroy(Sustentabilidade $registro, SustentabilidadeImagem $imagem)
    {
        try {

            $imagem->delete();
            return redirect()->route('painel.sustentabilidade.imagens.index', $registro)
                             ->with('success', 'Imagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: '.$e->getMessage()]);

        }
    }

    public function clear(Sustentabilidade $registro)
    {
        try {

            $registro->imagens()->delete();
            return redirect()->route('painel.sustentabilidade.imagens.index', $registro)
                             ->with('success', 'Imagens excluídas com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: '.$e->getMessage()]);

        }
    }
}
