<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\VideosRequest;
use App\Http\Controllers\Controller;

use App\Models\Videos;

class VideosController extends Controller
{
    public function index()
    {
        $registros = Videos::ordenados()->get();

        return view('painel.videos.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.videos.create');
    }

    public function store(VideosRequest $request)
    {
        try {

            $input = $request->all();

            Videos::create($input);

            return redirect()->route('painel.videos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Videos $registro)
    {
        return view('painel.videos.edit', compact('registro'));
    }

    public function update(VideosRequest $request, Videos $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.videos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Videos $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.videos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
