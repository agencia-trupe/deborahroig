<?php

namespace App\Http\Controllers;

use App\Models\Sobre;

class SobreController extends Controller
{
    public function index()
    {
        return view('frontend.sobre', [
            'sobre' => Sobre::first()
        ]);
    }
}
