<?php

namespace App\Http\Controllers;

use App\Models\Videos;

class VideoController extends Controller
{
    public function index()
    {
        $videos = Videos::ordenados()->get();

        return view('frontend.videos', compact('videos'));
    }
}
