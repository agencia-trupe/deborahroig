<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class MidiaRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'titulo' => 'required',
            'capa' => 'required|image',
            'descricao' => '',
            'link' => '',
            'video' => '',
        ];

        if ($this->method() != 'POST') {
            $rules['capa'] = 'image';
        }

        return $rules;
    }
}
