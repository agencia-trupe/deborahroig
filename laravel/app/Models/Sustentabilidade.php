<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Sustentabilidade extends Model
{
    protected $table = 'sustentabilidade';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function imagens()
    {
        return $this->hasMany('App\Models\SustentabilidadeImagem', 'sustentabilidade_id')->ordenados();
    }
}
