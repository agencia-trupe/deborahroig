<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class MidiaImagem extends Model
{
    protected $table = 'midia_imagens';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeMidia($query, $id)
    {
        return $query->where('midia_id', $id);
    }

    public static function uploadImagem()
    {
        return CropImage::make('imagem', [
            [
                'width'   => 180,
                'height'  => 180,
                'path'    => 'assets/img/midia/imagens/thumbs/'
            ],
            [
                'width'   => 1080,
                'height'  => null,
                'path'    => 'assets/img/midia/imagens/'
            ]
        ]);
    }
}
