<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSustentabilidadeTable extends Migration
{
    public function up()
    {
        Schema::create('sustentabilidade', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->text('descricao');
            $table->string('video');
            $table->timestamps();
        });

        Schema::create('sustentabilidade_imagens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sustentabilidade_id')->unsigned();
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->timestamps();
            $table->foreign('sustentabilidade_id')->references('id')->on('sustentabilidade')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('sustentabilidade_imagens');
        Schema::drop('sustentabilidade');
    }
}
