<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSobreTable extends Migration
{
    public function up()
    {
        Schema::create('sobre', function (Blueprint $table) {
            $table->increments('id');
            $table->string('imagem_1');
            $table->text('texto_1');
            $table->string('frase');
            $table->string('imagem_2');
            $table->text('texto_2');
            $table->string('imagem_3');
            $table->text('texto_3');
            $table->string('imagem_4');
            $table->text('texto_4');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('sobre');
    }
}
