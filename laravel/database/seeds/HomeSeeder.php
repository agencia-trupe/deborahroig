<?php

use Illuminate\Database\Seeder;

class HomeSeeder extends Seeder
{
    public function run()
    {
        DB::table('home')->insert([
            'frase' => '',
            'texto' => '',
            'video' => '',
            'imagem_sobre_1' => '',
            'imagem_sobre_2' => '',
        ]);
    }
}
